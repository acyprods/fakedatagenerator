import random
from faker import Faker
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey

from src.model.product import Product
from src.service.database import CONN, Initialize
from src.service.product_service import ProductService
from src.service.shop_service import ShopService
from src.service.product_category_service import ProductCategoryService
from src.service.product_shop_service import ProductShopService
from src.model.shop import Shop
from src.model.product_category import ProductCategory
from src.model.product_shop import get_class
from src import settings


# Creates table if not exists.
def create_table(table):
    try:
        engine = create_engine('postgres://{}:{}@{}:{}/{}'.format(settings.POSTGRES_USER, settings.POSTGRES_PASS,
                                                                  settings.POSTGRES_HOST, settings.POSTGRES_PORT,
                                                                  settings.POSTGRES_DB))
        table.create(engine)
    except Exception, exc:
        print exc


def generate_tables(product_shops_tables=1):
    meta = MetaData()
    product_category = Table('product_category', meta,
        Column('id', Integer, primary_key=True),
        Column('category_name', String(255), nullable=False, unique=True),
        Column('description', String(255))
    )

    shop = Table('shop', meta,
        Column('id', Integer, primary_key=True),
        Column('shop_name', String(255), nullable=False),
        Column('website', String(255), nullable=False),
        Column('description', String(255))
    )

    product = Table('product', meta,
        Column('id', Integer, primary_key=True),
        Column('origin_table', String(255), nullable=False)
    )

    product_shop_list = []
    for i in range(1, product_shops_tables):
        id = '{0:04}'.format(i)
        product_shop = Table('product_shop_id' + id, meta,
            Column('id', Integer, primary_key=True),
            Column('product_id', Integer, ForeignKey('product.id')),
            Column('name', String(255), nullable=False),
            Column('url', String(255), nullable=False),
            Column('price', String(20), nullable=False),
            Column('category_id', Integer, ForeignKey('product_category.id')),
            Column('images', String(255), nullable=True),
            Column('ean_code', String(15), nullable=True),
            Column('added_date', String(20), nullable=True),
            Column('updated_date', String(20), nullable=True)
        )
        product_shop_list.append(product_shop)

    create_table(product_category)
    create_table(shop)
    create_table(product)
    for product_shop in product_shop_list:
        create_table(product_shop)


def gen_shop_name():
    return Faker().company()


def gen_website():
    return Faker().uri()


def gen_ean():
    return Faker().ean(length=13)


def gen_product_name():
    return Faker().name()


def gen_price():
    return round(random.uniform(1, 1000), 2)


def gen_shop_records(amount=1):
    for x in range(0, amount):
        session = Initialize().create_session(CONN)
        shop = Shop(shop_name=gen_shop_name(), website=gen_website())
        ShopService(session).save(shop)
        session.close()


def create_product_record(origin_table):
    session = Initialize().create_session(CONN)
    last_id = ProductService(session).find_last_id()
    if not last_id:
        product_id = 1
    else:
        product_id = ProductService(session).find_last_id().id + 1
    product = Product(id=product_id, origin_table=origin_table)
    ProductService(session).save(product)
    session.close()
    return product_id


# Execute only once, or will throw an exception.
def insert_categories():
    try:
        for category in settings.PRODUCTS_CATEGORIES:
            session = Initialize().create_session(CONN)
            product_category = ProductCategory(category_name=category)
            ProductCategoryService(session).save(product_category)
            session.close()
    except Exception, exc:
        print 'Exception while inserting products categories.'


def gen_product_records(tablename, amount=1):
    for i in range(1, amount):
        session = Initialize().create_session(CONN)
        product_id = create_product_record(tablename)
        c = get_class(tablename)
        product = c(tablename=tablename, product_id=product_id, name=gen_product_name(), url=gen_website(), price=gen_price(),
                    category_id=random.randint(1, len(settings.PRODUCTS_CATEGORIES)))
        ProductShopService(session, tablename).save(product)
        session.close()


amount = 50  # number of product shop tables
generate_tables(amount)
gen_shop_records(amount)
insert_categories()
for i in range(1, amount):
    tablename = 'product_shop_id{}'.format('{0:04}'.format(i))
    gen_product_records(tablename, random.randint(400, 1000))

