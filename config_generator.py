from string import Template
import src.settings
from src.service.database import Initialize, CONN
from src.service.shop_service import ShopService


def generate_config(product_shop_table_name):
    id = int(product_shop_table_name.replace('product_shop_id', '').lstrip('0'))
    results = "\n" + Template(src.settings.INPUT_SCHEMA).substitute(dict(product_shop_table=product_shop_table_name))

    session = Initialize().create_session(CONN)
    shop = ShopService(session).find_by_id(id)
    d = dict(product_shop_table=product_shop_table_name, shop_id=shop.id, shop_name=shop.shop_name, shop_website=shop.website, shop_desc=shop.description)
    results += "\n" + Template(src.settings.FILTER_SCHEMA).substitute(d)
    results += "\n" + Template(src.settings.OUTPUT_SCHEMA).substitute(dict(product_shop_table=product_shop_table_name))

    return results


with open('product_shop_idxxxx.conf', 'a') as f:
    for x in range(1, 50):  # range(a, b) from a to x<b || (b-1)
        id = str(x).zfill(4)
        f.write(generate_config('product_shop_id{}'.format(id)))
        print 'product_shop_id{}'.format(id) + ',',

