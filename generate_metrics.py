import time
import random
import requests

from src.service.database import Initialize, CONN
from src.service.product_service import ProductService

schema = {
    "product_id": "$id",
    "origin_table": "$tablename",
    "metrics": []
}


def gen_price():
    return round(random.uniform(1, 1000), 2)


def generate_metrics(number=1):
    metrics = []
    timestamp = int(time.time())
    for num in xrange(number):
        rand_seconds = random.randint(86, 1000) * 10000  # from 86~= 1 day to 1000 days * 10000 seconds
        xy = [timestamp + rand_seconds, gen_price()]  # [timestamp, price]
        metrics.append(xy)
    return metrics


session = Initialize().create_session(CONN)
products = ProductService(session).fetch_all()
session.close()
for product in products:
    schema["product_id"] = product.id
    schema["origin_table"] = str(product.origin_table)
    schema['metrics'] = generate_metrics(random.randint(40, 55))
    response = requests.post('http://project.test:9200/product_metrics/_doc/', json=schema)

