from src.model.shop import Shop


class ShopService(object):
    def __init__(self, session):
        self.Shop = Shop
        self.session = session

    def find_by_id(self, id):
        return self.session.query(self.Shop).filter(self.Shop.id == id).first()

    def save(self, shop):
        self.session.add(shop)
        self.session.flush()
        self.session.commit()

