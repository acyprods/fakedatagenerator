from sqlalchemy_dao import Dao
from sqlalchemy.ext.declarative import declarative_base
from src import settings


class DatabaseService(object):
    _conn = None

    @property
    def get_conn(self):
        if not self._conn:
            self._conn = Dao('postgres://{}:{}@{}:{}/{}'.format(settings.POSTGRES_USER, settings.POSTGRES_PASS,
                                                                settings.POSTGRES_HOST, settings.POSTGRES_PORT,
                                                                settings.POSTGRES_DB))
        return self._conn


Base = declarative_base()
prop = DatabaseService()
CONN = prop.get_conn


class Initialize(object):
    def __init__(self):
        pass

    def create_session(self, conn):
        return conn.create_session()

