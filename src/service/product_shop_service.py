from src.model.product_shop import get_class


class ProductShopService(object):
    def __init__(self, session, tablename):
        self.ProductShop = get_class(tablename)
        self.session = session

    def save(self, product_shop):
        self.session.add(product_shop)
        self.session.flush()
        self.session.commit()

