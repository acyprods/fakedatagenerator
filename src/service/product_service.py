from src.model.product import Product


class ProductService(object):
    def __init__(self, session):
        self.Product = Product
        self.session = session

    def find_last_id(self):
        return self.session.query(self.Product).order_by(self.Product.id.desc()).first()

    def fetch_all(self):
        return self.session.query(self.Product).all()

    def save(self, product):
        self.session.add(product)
        self.session.flush()
        self.session.commit()
