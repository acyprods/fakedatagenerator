from src.model.product_category import ProductCategory


class ProductCategoryService(object):
    def __init__(self, session):
        self.ProductCategory = ProductCategory
        self.session = session

    def save(self, category):
        self.session.add(category)
        self.session.flush()
        self.session.commit()

