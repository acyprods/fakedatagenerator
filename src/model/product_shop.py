from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base


def get_class(tablename):
    Base = declarative_base()

    class Product(Base):
        __tablename__ = 'product'
        id = Column(Integer, primary_key=True)
        origin_table = Column(String, nullable=False)

        def __init__(self, id=None, origin_table=None):
            self.id = id
            self.origin_table = origin_table

    class ProductCategory(Base):
        __tablename__ = 'product_category'
        id = Column(Integer, primary_key=True)
        category_name = Column(String, nullable=False, unique=True)
        description = Column(String, nullable=False)

        def __init__(self, id=None, category_name=None, description=None):
            self.id = id
            self.category_name = category_name
            self.description = description

    class ProductShop(Base):
        __tablename__ = tablename
        id = Column(Integer, primary_key=True)
        product_id = Column(Integer, ForeignKey('product.id'))
        name = Column(String, nullable=False)
        url = Column(String, nullable=False)
        price = Column(String, nullable=False)
        category_id = Column(Integer, ForeignKey('product_category.id'))
        images = Column(String, nullable=True)
        ean_code = Column(String, nullable=True)
        added_date = Column(String, nullable=True)
        updated_date = Column(String, nullable=True)

        def __init__(self, tablename=None, id=None, product_id=None, name=None, url=None, price=None, category_id=None,
                     images=None, ean_code=None, added_date=None, updated_date=None):
            self.__tablename__ = tablename
            self.id = id
            self.product_id = product_id
            self.name = name
            self.url = url
            self.price = price
            self.category_id = category_id
            self.images = images
            self.ean_code = ean_code
            self.added_date = added_date
            self.updated_date = updated_date

    return ProductShop
