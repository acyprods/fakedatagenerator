from sqlalchemy import Integer, Column, String
from src.service.database import Base


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    origin_table = Column(String, nullable=False)

    def __init__(self, id=None, origin_table=None):
        self.id = id
        self.origin_table = origin_table
