from sqlalchemy import Column, Integer, String
from src.service.database import Base


class Shop(Base):
    __tablename__ = 'shop'
    id = Column(Integer, primary_key=True)
    shop_name = Column(String, nullable=False)
    website = Column(String, nullable=False)
    description = Column(String, nullable=True)

    def __init__(self, id=None, shop_name=None, website=None, description=None):
        self.id = id
        self.shop_name = shop_name
        self.website = website
        self.description = description
