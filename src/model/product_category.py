from sqlalchemy import Column, Integer, String
from src.service.database import Base


class ProductCategory(Base):
    __tablename__ = 'product_category'
    id = Column(Integer, primary_key=True)
    category_name = Column(String, nullable=False, unique=True)
    description = Column(String, nullable=False)

    def __init__(self, id=None, category_name=None, description=None):
        self.id = id
        self.category_name = category_name
        self.description = description

