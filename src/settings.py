POSTGRES_USER = 'postgres'
POSTGRES_PASS = '12345'
POSTGRES_HOST = '192.168.200.78'
POSTGRES_PORT = '5432'
POSTGRES_DB = 'acyprods_products'

PRODUCTS_CATEGORIES = ['Animals & Pet Supplies',
                        'Apparel & Accessories',
                        'Arts & Entertainment',
                        'Baby & Toddler',
                        'Business & Industrial',
                        'Cameras & Optics',
                        'Electronics',
                        'Food, Beverages & Tobacco',
                        'Furniture',
                        'Hardware',
                        'Health & Beauty',
                        'Home & Garden',
                        'Luggage & Bags',
                        'Mature',
                        'Media',
                        'Office Supplies',
                        'Religious & Ceremonial',
                        'Software',
                        'Sporting Goods',
                        'Toys & Games',
                        'Vehicles & Parts']


INPUT_SCHEMA = """input { 
  jdbc {
    type => "$product_shop_table"
    jdbc_driver_library => "/usr/postgres/postgresql-42.2.2.jar"
    jdbc_driver_class => "org.postgresql.Driver"
    jdbc_validate_connection => true
    jdbc_connection_string => "jdbc:postgresql://postgresql:5432/acyprods_products"
    jdbc_user => "postgres"
    jdbc_password => "12345"
    schedule => "*/10 * * * * *"
    statement => "SELECT * FROM $product_shop_table WHERE id > :sql_last_value"
    use_column_value => true
    tracking_column => "id"
    tracking_column_type => "numeric"
    last_run_metadata_path => "/usr/share/logstash/.logstash_jdbc_last_run_$product_shop_table"
    clean_run => true
  }
}"""

FILTER_SCHEMA = """filter {
    if [type] == "$product_shop_table" {
        mutate {   
            add_field => {
                "[shop][id]" => $shop_id
                "[shop][shop_name]" => "$shop_name"
                "[shop][website]" => "$shop_website"
                "[shop][description]" => "$shop_desc"
            }
        }
    }
}"""

OUTPUT_SCHEMA = """output {
  if [type] == "$product_shop_table" {
    elasticsearch { 
        hosts => ["elasticsearch:9200"] 
        index => "$product_shop_table"
        document_id => "%{id}"
    }
  }
}"""