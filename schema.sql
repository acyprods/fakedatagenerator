-- noinspection SqlNoDataSourceInspectionForFile


psql -U postgres -h localhost -d acyprods_products

CREATE DATABASE acyprods_products;

CREATE TABLE shop (
	id SERIAL PRIMARY KEY,
	shop_name varchar(255) NOT NULL UNIQUE,
	website varchar(255) NOT NULL,
	description varchar(255)
);


CREATE TABLE product_category (
	id SERIAL PRIMARY KEY,
	category_name varchar(255) NOT NULL UNIQUE,
	description varchar(255)
);


CREATE TABLE product_shop_id0643 (
	id SERIAL PRIMARY KEY,
	product_uid int NOT NULL REFERENCES product(id),
	name varchar(255) NOT NULL,
	url varchar(255) NOT NULL,
	price varchar(255) NOT NULL,
	category_id int NOT NULL REFERENCES product_category(id),
	images varchar(255),
	ean_code varchar(255),
	added_date varchar(255),
	updated_date varchar(255)
);

CREATE TABLE product (
  id SERIAL PRIMARY KEY,
  origin_table varchar(255) NOT NULL
);
